# Python3 Refresher Notes

## Objects and data structures
### Variable assignment
  - cant use &#(#$
  - PEP8 convention -  lowercase names
  - dynamically typed (note: strings add vs int add)
  - can be reassigned i.e :
    ``` python
      a = 5,
      a = a + a ,
      print(a) # 10
    ```

### Strings
  - Strings are **ordered sequences** thus we can use **indexing** or **slicing** to grab sub-sections i.e can use
        [index] or [start:stop:step] or len()
        excludes stop or [::2]
  - usual escape sequences
        /n newline /t tab
  - strings are immutable, cant use **"james"[2] = k** to change the individual elements of the sting

  #### **string formatting**
    ```python
    print('This is a string {}'.format('inserted'))
    print('The {0} {0} {0}'.format('fox', 'brown','quick'))
    print('The {q} {b} {f}'.format(f = 'fox',b ='brown',q ='quick'))
    ```
  - float formatting: "{value:width.precision *f* }"
    ```python
    result = 0.1287001
    print("result was {r:1.3f}".format(r=result))
    # the result was 0.129
    # r:10.3f will result in whitespace
    ```
  - f-strings literals (python 3.6, or import \__future__)
    ```python
    name = Jose
    print(f'hello his name is {name}')
    # hello his name is Jose
    ```

### Lists
  - can have multiple obj types
  - list concatenation
  ```python
  list = ['hi','there']
  list2 = ['stranger']
  list + list2  # ['hi','there','stranger']
  ```
  - Mutable (unlike strings) i.e `newlist[3] = hi` will change

  - Remove
    `.pop()`, can also be assigned i.e `last = list.pop()`
    or `.pop(index)`

  - code occur **in place** , thus assignment + method will not work
  i.e `my_sorted_list = new_list.sorted()` will not assign my_sorted_list with sorted new_list although new_list is now sorted. Instead:
  ```python
  new_list.sorted()
  my_sorted_list = new_list
  ```
  https:#stackoverflow.com/questions/5317817/python-in-place-functions

### Dictionaries
  - kv pair `{'Key':'Val'}`
  - flexible, can hold multi-types int, lists, dict and stacked to retrieved
    ```
    dict1 = {'A1':123, 'A2':[3,2,4],'A3':{'insideKey':100}}
    d['A2'] # [3,2,4]
    d['A2'][2] # 4
    d['A3'] # {'insideKey':100}
    d['A3']['insideKey'] # 100

    ```
    #whats the implementation like internally for dict?
  - Dictionaries are mappings and do not retain order
  - Unsequenced so can't be indexed or sliced, Objects are retrieved by **key name** instead
  ```python
  price_lookup = {'apple': 1, 'banana':1.2}
  price_lookup['apple'] # 1
  ```
  - can add/override KV pairs
  ```python
  d = {'k':1,'j':2}
  d['k'] = 2 # {'k':2,'j':2}
  d['l'] = 3 # {'k':2,'j':2,'l':3}
  ```
  - methods for k/v only retrieval, or pairings(in tuple form)
  ```python
  d.keys() d.values() d.items()
  ```

### Tuples
  - like lists but immutable and less methods
  - for data integrity (i.e source of truth)
  ```python
  t = ('one', 2)
  t[0] # one
  t[-1] # 2
  t = ("a", "a","a")
  t.count('a') # 2
  t.index('a')  # returns index of first "a", 0
  t[0] # returns TypeError, 'tuple' obj does not support item assignment
  ```

### Sets
  - Unique values only, useful for casting list into set to get only unique values, and no order/sequence, and does not support indexing i.e. result[1]
  ```python
  ##### set() Constructor  ####
  myset = set() # also can
  myset.add(1) # only takes in one arg
  myset # returns {1}
  myset.add([1,2,3,4,4,4]) # TypeError: unhashable type: 'list'
  myset.update(["orange", "mango", "grapes"]) # supports multi elems

  set([1,2,3,4,4,4,4]) # returns 1,2,3,4
  set("Missisippi") # returns 'i', 'p', 's', 'M'

  ### removing things ###
  myset.remove("james") #method will raise error if item does not exist
  myset.discard("james") # but will not
  myset.pop() #will remove last item but since sets are unordered, you don't know what you'll get
  del myset #to delete set

  ```
  for more methods : https://www.w3schools.com/python/python_sets.asp

### Booleans & comparison operators
  - True/False , must be capitalised
  - b = None  #to initialise

-----


### I/O with Basic files in Python
- creating txt file in jupyter notebook
`%%writefile myfile.txt
 whatever u wanna
 write in plain text`

- I/O methods
```python
  myfile.open('myfile.txt') #filepath depends on OS, and have to close after
  myfile.read() #returns contents in single strings, inclds esc sequences
  myfile.seek(0) # resets cursor to top of contents to grab again
  myfile.readlines() # eachline as a separate object in a list ["whatever u wanna\n", "whatever u wanna"]
  myfile.close()

  ## alternatively
  with open('myfile.txt') as new_file_variable:
      contents = new_file_variable.read

  ## using modes to limit permissions    
  # mode = 'r' read only
  # mode = 'w' write only
  # mode = 'a' append only
  # mode = 'r+' read and write
  # mode = 'w+' writing and reading (override existing file or creates new file if file doesn't exists)

  ## Eg:
  with open('test.txt',mode='w') as f:
    f.write('Hello World')
```

-----
### Logical Operators

- https://www.programiz.com/python-programming/operators

- even more detailed
https://thomas-cokelaer.info/tutorials/python/boolean.html

---

### Control flows
- Control Flow syntax makes use of colons and indentations(whitespace). Pretty standard stuff:
```python
  if some_condition:
    #execute some code
  elif some_other_condition:
    #some
  else:
    #code
```

- Iterables

  ```python
    my_iterable = [1,2,3,5,6,3]
    for item in my_iterable:
      #check for even
      if item % 2 == 0:
        print(item)
      else:
        print(f'Odd Number: {item}')

    #Print indiv letter
    for letter in "Hello World":
      print(letter)
    ```
    ```python
    #Tuple unpacking
    tup = (1,2,3)
    for item in tup:
      print (item)

    mylist = [(1,2),(3,4),(5,6)] # list of tuples
    for a,b in tup:
      print (a)
      print (b)
    # returns 1 2 3 4 5
    ```
    ```python
    ## Dictionaries, note that they're unordered
    d = {'K1':1,'K2':2,'K3':3}
    for key,value in d.items():
      print(value)

    ##Alternatively
    for value in d.values():
      print(value)
    ```
- while loops
    ```python
    x = 0
    while x < 5:
      print(f'The curr val of x is {x}')
      x += 1
    else:
      print('X IS 5')
    ```
    ```python
      break # Breaks out of CURRENT, CLOSEST enclosing loop
      continue # Goes to the TOP of CLOSEST enclosing loop
      pass # Does nothing at all.

      ### Pass
      x = [1,2,3]
      for item in x:
        #comment
        pass #placeholder when doing incomplete loop to avoid errors

      ### Continue
      mystring = "sammy"
      for letter in mystring:
        if letter == "a":
          continue
        print(letter) ## will not print a , goes back to "for" and move on to next letter

      ### Break
      x = 0
      while x < 5
        if x==2:
          break ## while loop stops when cond. is met
        print(x)
        x += 1
    ```

- Other useful operators
    ```python
    mylist = [1,2,3]
    for num in range(0,10,2): ## range(start,stop,step) does not incld stop
      print(num)
    ## also
    list(range(0,11,2)) #pack into list
    [0,2,3,4,6,8,19]
    ```
    ```python
    ### Using enumerate

    # Usually we do this:
    index_count = 0
    for letter in 'abcde':
      print(f'index is {} at letter {}'.format(index_count,letter))
      index_count += 1

    #But it can also be like this:
    index_count = 0
    word = 'abcde'
    for letter in word:
      print(word[index_count])
      index_count += 1

    # With enumerate it can be like this:
    word = 'abcde'
    for item in enumerate(word):
      print(item)
      #returns Tuples
      #(0,'a')
      #(1,'b') ...

    # And like any tuples you can:
    word = 'abcde'
    for index,letter in enumerate(word):
      print('the index is {} for letter {}'.format(index,letter))
    ```
    ```python
    ### Using Zip

    list1 = [1,2,3]
    list2 = ['a','b','c']

    for item in zip(mylis1,mylist2):
      print item
      #zip packs two lists tgt and also returns tuples
      #(1,'a')
      #(2,'b')
      #(3,'c')
      #it can zip > 2 lists and combines indices up to the shortest list

    ### in keyword
    ### min(mylist)
    ### max(mylist)
    ```
- importing from random lib
    ```python
    from random import shuffle
    mylist = [1,2,3,4,5,6,7,8,9,10]
    shuffle(mylist) #jumbles up sequence, also an in-place method so cant so assignment in same line i.e shuffled = suffle(mylist), shuffled will be NoneType

    from random import randint
    randint(0,100)
    ```
- using user input
    ```python
    result = input('Whats your age: ') #type(result) will be str

    ## cast/transform type
    int(result) or float(result)
    ```
- list comprehension
  ```python
  mystring = 'hello'
  mylist = []
  for letter in mystring:
    mylist.append(letter) #['h', 'e', 'l', 'l', 'o']

  # can also be
  mylist = [letter for letter in mystring] #['h', 'e', 'l', 'l', 'o']

  # eg:
  mylist = [x for x in 'word'] # ['w', 'o', 'r', 'd']

  # eg:
  mylist = [num**2 for num in range(0,11)] #[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

  # eg:
  mylist = [x for x in range(0,11) if x%2==0] #[0, 2, 4, 6, 8, 10]

  #eg:
  celcius = [0,10,20,34.5]
  fahrenheit = [((9/5)*temp + 32) for temp in celcius] # same thing but longer operations are enclosed in brackets
  print(fahrenheit) #returns [32, 42, 52, 66.5]

  #also represented as
  fahrenheit = []
  for temp in celcius:
    fahrenheit.append((9/5)*temp + 32))

  #If u like it messy:
  results = [x if x%2==0 else 'ODD' for x in range(0,11)]
  #returns [0, 'ODD', 2, 'ODD', 4, 'ODD', 6, 'ODD', 8, 'ODD', 10]

  #nested loops for upsized messiness

  mylist = []

  for x in [2,4,6]:
    for y in [1,100,300]:
      mylist.append(x*y) #[200,400,600,400,800,1200,600,1200,1800]

  #Alternatively
  mylist = [x*y for x in [2,4,6] for y in [1,10,1000]]

  ```
  ```python
    #using help()
    help(k.sort) # to get documentation
  ```
-----
### Writing Functions in Python
- With default values
```python
  def function_name(param= 'defaultval'):
    print(param)
    return(param) #return to assign
```
```python
## Beginner's mistake
  def dog_check(mystring):
    if 'dog' in mystring.lower():
      return True
    else:
      return False
## "X in Y" is alr a boolean
  def dog_check(mystring):
    return 'dog' in mystring.lower()
```
-Pig Latin
```python
def pig_latin(word):
  first_letter = word[0]
  #check for vowel
  if first_letter in 'aeiou':
    pigword = word + ay
  else:
    pigword = word[1:] + first_letter + 'ay'

  return pigword
```

### \*args and **kwargs
```python
```
